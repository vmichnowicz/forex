/**
 * Pad string on the right
 *
 * @param string
 * @param int
 * @return String
 * @url <http://sajjadhossain.com/2008/10/31/javascript-string-trimming-and-padding/>
 */
String.prototype.rpad = function(string, length) {
	var str = this;
    while (str.length < length)
        str = str + string;
    return str;
}

$(document).ready(function() {

	// Get country and exchange rate data
	$.get('data', function(data) {

		$('#countries').empty();

		// Loop through each country
		$.each(data, function(countryCode, country) {
			var el = $('<div class="country" id="' + countryCode + '"/>');
			var heading = $('<h3>' + country.name + ' <em><span class="rate">' + country.rate + '</span> <span class="currency_code">' + country.currency_code + '</span></em></h3>');
			var news = $('<div class="news" style="display: none;"></div><a href="javascript:void(0);" class="latest_news">Latest News</a>');
			$(el).append(heading, news);
			$('#countries').append(el);
		});

		setMoods();
	}, 'json');

	// On latest news click
	$('body').on('click', '#countries a.latest_news', function(e) {
		e.preventDefault();
		var button = $(this);
		var country = $(this).closest('div.country');
		var countryCode = $(country).attr('id');
		var news = $(country).find('div.news');

		if ( $(button).attr('disabled') ) { return; }

		$(button).attr('disabled', true);

		// If news is visible
		if ( $(news).is(':visible') ) {
			// Hide and then empty
			$(news).slideUp('fast', function() {
				$(this).empty();
				$(button).removeAttr('disabled');
			});
		}
		// If news is not visible
		else {
			$.get('/news/' + countryCode, function(data) {
				var ul = $('<ul />');
				$.each(data, function(index, article) {
					
					var li = $('<li />');
					var title = $('<h4>' + article.title + '</h4>');
					var links = $('<span></span>');

					var sad = $('<a href="/mood/sad/' + countryCode + '/' + article.url_hash + '" class="sad ' + (article.mood_user == 0 ? 'current' : 'inactive') + '" title="Sad">Sad</a>');
					var neutral = $('<a href="/mood/neutral/' + countryCode + '/' + article.url_hash + '" class="neutral ' + (article.mood_user == 1 ? 'current' : 'inactive') + '" title="Neutral">Neutral</a>');
					var happy = $('<a href="/mood/happy/' + countryCode + '/' + article.url_hash + '" class="happy ' + (article.mood_user == 2 ? 'current' : 'inactive') + '" title="Happy">Happy</a></span>');

					$(links).append(sad, neutral, happy);
					$(li).append(title, links);
					
					$(ul).append(li);
				});
				$(news).append(ul).show().hide().slideDown('fast');
				$(button).removeAttr('disabled');
			});
		}
	});

	// On mood click
	$('body').on('click', '#countries .news ul a', function(e) {
		e.preventDefault();
		
		var link = $(this);
		var links = $(this).closest('span').find('a');

		if ( $(link).attr('disabled') ) { return; }

		$(link).attr('disabled', true);

		$.get($(this).attr('href'), function(data) {
			if (data == true) {
				$(links).removeClass('active inactive');
				$(links).addClass('inactive');
				$(link).addClass('active');
			}

			$(link).removeAttr('disabled');

			setMoods();
		});
	});

	var setMoods = function setMoods() {

		$.get('/moods', function(data) {
			console.log(data);

			$('#countries .country').each(function() {
				var country = $(this);
				var countryCode = $(this).attr('id');
				var currencyCode = $(this).find('span.currency_code').text();
				var rate = $(this).find('span.rate').text();
				$(data).each(function(index, currency) {
					if ( currency.currency_code == currencyCode ) {
						$(country).find('span.rate').removeClass().addClass('rate mood_' + currency.mood_scaled);
					}
				});
				
			});
		});

		return setMoods;
	};

});