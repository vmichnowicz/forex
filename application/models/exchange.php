<?php

class Exchange {

	/**
	 * Get all curriencies
	 *
	 * @access public
	 * @return Response
	 * @static
	 */
    public static function get_currencies()
    {
		// Attempt to get currencies from cache
		$currencies = Cache::get('currencies');

		$data = DB::table('currencies')->get();

		if ( is_array($data) )
		{
			foreach ($data as $currency)
			{
				$currencies[ $currency->currency_code ] = $currency->currency_name;
			}

			Cache::put('currencies', $currencies, 15); // Store for 15 minutes
		}

		return $currencies;
    }

	/**
	 * Get all exchange rates based on 1 USD
	 *
	 * @access public
	 * @return Response
	 * @static
	 */
	public static function get_rates()
	{
		// Attempt to get rates from cache
		$rates = Cache::get('rates');

		// If we don't have rates in cache
		if ( empty($rates) )
		{
			// Open CURL session:
			$handle = curl_init('http://openexchangerates.org/api/latest.json?app_id=' . Config::get('api.exchange'));
			curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);

			// Get the data:
			$json = curl_exec($handle);
			curl_close($handle);

			$data = json_decode($json, TRUE);

			if ( isset($data['rates']) AND is_array($data['rates']) )
			{
				$rates = $data['rates'];
				Cache::put('rates', $rates, 15); // Store for 15 minutes
			}
		}

		return $rates;
	}

	/**
	 * Return contatination of country and currency mapping
	 *
	 * @access public
	 * @return Response
	 * @static
	 */
	public static function get_data()
	{
		// Attempt to get rates from cache
		$data = Cache::get('data');

		if ( empty($data) )
		{
			$mapping = Config::get('mapping');
			$rates = self::get_rates();
			$currencies = self::get_currencies();

			// Make sure we have arrays of mapping, rates, and currencies
			if ( is_array($mapping) AND is_array($rates) AND is_array($currencies) )
			{
				// Loop through all our rates
				foreach($rates as $currency_code => $rate)
				{
					// Loop through all our mappings
					foreach($mapping as $country_code => &$country)
					{
						// If this country uses the current currency
						if ( isset($country['currency_code']) AND strtoupper($country['currency_code']) === strtoupper($currency_code) )
						{
							$country['rate'] = $rate;
						}
					}
				}

				// Loop through each currency
				foreach ($currencies as $currency_code => $currency_name)
				{
					// Loop through all our mappings
					foreach($mapping as $country_code => &$country)
					{
						// If this country uses the current currency
						if ( isset($country['currency_code']) AND strtoupper($country['currency_code']) === strtoupper($currency_code) )
						{
							$country['currency_name'] = $currency_name;
						}
					}
				}

				$data = $mapping;
				Cache::put('data', $data, 15); // Store for 15 minutes
			}
		}

		return $data;
	}

}