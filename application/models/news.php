<?php

class News {

	const MOOD_SAD = 'sad'; // 0
	const MOOD_NEUTRAL = 'neutral'; // 1
	const MOOD_HAPPY = 'happy'; // 2

	public static $moods = array(self::MOOD_SAD, self::MOOD_NEUTRAL, self::MOOD_HAPPY);

	/**
	 * Get news for a particular geographic area
	 *
	 * @access public
	 * @param string $geo_facet
	 * @return array
	 * @static
	 */
	public static function get_news($geo_facet)
	{
		// Cache key
		$key = 'news_' . strtolower($geo_facet);

		// Attempt to get news from cache
		$news = Cache::get($key);

		// If news data does not exist in cache
		if ( empty($news) )
		{
			// URL paramaters
			$params = array(
				'format' => 'json',
				'query' => 'geo_facet:[' . $geo_facet . ']',
				'fields' => 'byline,date,title,url,org_facet,geo_facet',
				'api-key' => Config::get('api.news')
			);

			$handle = curl_init('http://api.nytimes.com/svc/search/v1/article?' . http_build_query($params) );
			curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);

			// Get the data:
			$json = curl_exec($handle);
			curl_close($handle);

			$data = json_decode($json, TRUE);

			// Make sure we have rate data
			if ( array_key_exists('results', $data) AND is_array($data['results']) )
			{
				$news = $data['results']; // Grab rates from return data

				// Loop through each article
				foreach($news as &$article)
				{
					$url = isset($article['url']) ? $article['url'] : NULL;

					if ($url)
					{
						// Add md5 version of article URL
						$article['url_hash'] = md5($article['url']);
					}
				}

				Cache::put($key, $news, 15); // Store for 15 minutes
			}
		}

		// If we have an array of news (we do not want to cache this data)
		if ( is_array($news) )
		{
			// Loop through each news article
			foreach($news as &$article)
			{
				$url_hash = isset($article['url_hash']) ? $article['url_hash'] : NULL;

				if ($url_hash)
				{
					$article['mood_average'] = self::get_average_mood($url_hash);
					$article['mood_user'] = self::get_user_mood($url_hash);
				}
			}
		}

		return $news;
	}

	/**
	 * Get a mood value
	 *
	 * @access public
	 * @param string $mood
	 * @return int
	 * @static
	 */
	public static function get_mood_value($mood)
	{
		foreach (self::$moods as $mood_value => $mood_text)
		{
			if ($mood === $mood_text)
			{
				return $mood_value;
			}
		}
	}

	/**
	 * Set the mood for a country and article
	 *
	 * @access public
	 * @param string $mood
	 * @param string $country_code
	 * @param string $currency_code
	 * @param string $url_hash
	 * @return bool
	 * @static
	 */
	public static function set_mood($mood, $country_code, $currency_code, $url_hash)
	{
		$existing = DB::table('moods')
			->where('session_id', '=', Session::$instance->session['id'])
			->where('url_hash', '=', $url_hash)
			->get( array('id') );

		$data = array(
			'country_code' => strtoupper($country_code),
			'mood' => self::get_mood_value($mood),
			'url_hash' => $url_hash,
			'session_id' => Session::$instance->session['id']
		);

		// If user has already set a mood for this article
		if ( is_array($existing) AND ! empty($existing) )
		{
			$id = array_shift($existing);
			$id = $id->id;

			DB::table('moods')
				->where('id', '=', $id)
				->update($data);
		}
		// If this user have not already set a mood
		else
		{
			DB::table('moods')->insert($data);
		}

		return TRUE;
	}

	/**
	 * Get the average mood for a URL
	 *
	 * @access public
	 * @param type $url_hash
	 * @return int
	 * @static
	 */
	public static function get_average_mood($url_hash)
	{
		$data = DB::query('SELECT AVG(mood) AS mood FROM moods WHERE url_hash = ?', array($url_hash) );

		if ( is_array($data) AND count($data) > 0 )
		{
			$return = array_shift($data);
			$return = property_exists($return, 'mood') ? $return->mood : NULL;
			return $return;
		}
	}

	/**
	 * Get user mood for a given article
	 *
	 * @access public
	 * @param string $url
	 * @return int
	 * @static
	 */
	public static function get_user_mood($url_hash)
	{
		$data = DB::table('moods')
				->where('session_id', '=', Session::$instance->session['id'])
				->where('url_hash', '=', $url_hash)
				->get( array('mood') );

		if ( is_array($data) AND count($data) > 0 )
		{
			$return = array_shift($data);
			$return = property_exists($return, 'mood') ? $return->mood : NULL;
			return $return;
		}
	}

	/**
	 * Get compiled moods per currency
	 *
	 * @access public
	 * @param array $config
	 * @return array
	 * @static
	 */
	public static function get_moods()
	{
		$data = DB::query('
			SELECT countries.currency_code, AVG(mood) mood, ( round( AVG(mood) * 5 ) ) mood_scaled
			FROM moods, countries
			WHERE timestamp > ADDDATE(NOW(), INTERVAL -1 WEEK) AND countries.country_code = moods.country_code
			GROUP BY currency_code
		');

		return $data;
	}
}