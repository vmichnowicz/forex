<!doctype html>
<html ng-app="forex" lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Forex</title>
	<meta name="viewport" content="width=device-width">

	<!-- CSS -->
	<link rel="stylesheet" href="/css/style.css" type="text/css">

	<!-- jQuery -->
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
</head>
<body>

	<div>
		<header>
			<h1>Forex Feelings <em>Helping to determine the mood of the world&rsquo;s currencies</em></h1>
		</header>
		<div id="countries"><div class="loading">Loading&hellip;</div></div>
		<footer>Prices based on one USD, Copyright &copy; 2012 Victor Michnowicz</footer>
	</div>

	<!-- JavaScript -->
	<script type="text/javascript" src="js/script.js"></script>

</body>
</html>
