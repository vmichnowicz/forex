<?php

return array(
	// United States
	'us' => array(
		'currency_code' => 'USD',
		'geo_facet' => 'UNITED STATES',
		'name' => 'United States'
	),
	// China
	'cn' => array(
		'currency_code' => 'CNY',
		'geo_facet' => 'CHINA',
		'name' => 'China'
	),
	// Germany
	'de' => array(
		'currency_code' => 'EUR',
		'geo_facet' => 'GERMANY',
		'name' => 'Germany'
	),
	// France
	'fr' => array(
		'currency_code' => 'EUR',
		'geo_facet' => 'FRANCE',
		'name' => 'France'
	),
	// United Kingdom
	'uk' => array(
		'currency_code' => 'EUR',
		'geo_facet' => 'UNITED KINGDOM',
		'name' => 'U.K.'
	),
	// Brazil
	'br' => array(
		'currency_code' => 'BRL',
		'geo_facet' => 'BRAZIL',
		'name' => 'Brazil'
	),
	// Italy
	'it' => array(
		'currency_code' => 'EUR',
		'geo_facet' => 'ITALY',
		'name' => 'Italy'
	),
	// India
	'in' => array(
		'currency_code' => 'INR',
		'geo_facet' => 'INDIA',
		'name' => 'India'
	),
	// Canada
	'ca' => array(
		'currency_code' => 'CAD',
		'geo_facet' => 'CANADA',
		'name' => 'Canada'
	),
	// Russia
	'ru' => array(
		'currency_code' => 'RUB',
		'geo_facet' => 'RUSSIA',
		'name' => 'Russia'
	)
);
