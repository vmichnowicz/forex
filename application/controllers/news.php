<?php

/**
 * Class to retrieve news for various countries
 */
class News_Controller extends Base_Controller
{
	/**
	 * Get news as JSON
	 *
	 * @access public
	 * @param string $country_code
	 * @return Response
	 */
	public function action_news($country_code)
	{
		// Get config data for this country
		$config = Config::get('mapping.' . $country_code);

		// If this country has a legit geo_facet
		if ( $config AND isset($config['geo_facet']) )
		{
			// Get news for this country
			$news = News::get_news( $config['geo_facet'] );
			return Response::json($news);
		}
	}

	/**
	 * Set the mood for a countries article
	 *
	 * @access public
	 * @param string $mood
	 * @param string $country_code
	 * @param string $url_hash
	 * @return Response
	 */
	public function action_mood($mood, $country_code, $url_hash)
	{
		// Get config data for this country
		$config = Config::get('mapping.' . $country_code);

		// If this config was found, URL was decoded properly, and we have a valid mood
		if ( $config AND $url_hash AND in_array($mood, News::$moods) )
		{
			// Set mood
			if ( News::set_mood($mood, $country_code, $config['currency_code'], $url_hash) )
			{
				return Response::json(TRUE);
			}
		}

		return Response::json(FALSE);
	}

	/**
	 * Get moods for each country
	 *
	 * @access public
	 * @return Response
	 */
	public function action_moods()
	{
		return Response::json( News::get_moods() );
	}
}