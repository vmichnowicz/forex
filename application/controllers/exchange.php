<?php

/**
 * Class to retrieve exchange rates and list of curriencies
 */
class Exchange_Controller extends Base_Controller
{
	/**
	 * Return JSON currencies
	 *
	 * @access public
	 * @return Response
	 */
    public function action_currencies()
    {
		$currencies = Exchange::get_currencies();
		return Response::json($currencies);
    }

	/**
	 * Return JSON rates
	 *
	 * @access public
	 * @return Response
	 */
	public function action_rates()
	{
		$rates = Exchange::get_rates();
		return Response::json($rates);
	}

	/**
	 * Return JSON contatination of country and currency mapping
	 *
	 * @access public
	 * @return Response
	 */
	public function action_data()
	{
		$data = Exchange::get_data();
		return Response::json($data);
	}

}